/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import { Dropdown } from "bootstrap";

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';

document.addEventListener('DOMContentLoaded', () => {
  new App();
});


class App {
  constructor() {
    this.enableDropdows();
    this.handleCommentForm();
  }

  enableDropdows = () => {
    const dropdownElementList = [].slice.call(document.querySelectorAll('.dropdown-toggle'))
    dropdownElementList.map(function (dropdownToggleEl) {
      return new Dropdown(dropdownToggleEl)
    });
  }

  handleCommentForm() {

    const commentForm = document.querySelector('form.comment-form');

    if (null == commentForm) {
      return;
    }

    commentForm.addEventListener('submit', async (e) => {
      e.preventDefault();

      const response = await fetch('/ajax/comment', {
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        method: 'POST',
        body: new FormData(e.target)
      })
      if (!response) {
        return;
      }

      const json = await response.json();

      if(json.code === 'COMMENT_ADDED_SUCCESSFULLY'){
        const commentList = document.querySelector('.comment-list');
        console.log(commentList);
        const commentCount = document.querySelector('.comment-count');
        const commentContent = document.querySelector('#comment_content');

        commentList.insertAdjacentHTML('beforeend', json.message);
        commentList.lastElementChild.scrollIntoView();
        commentCount.innerText = json.numberOfComments;
        commentContent.value = '';

      }

    })
  }
}